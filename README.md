UNITY SAMPLE PROJECT

This project was created for VXR-1170: Game Engine Coding I. It will be used for learning and as the basis for a number of assignments in the course.

This project was built with Unity 2019.1.6.

You will need to download this repository and create a new repository in your BitBucket account with these files as the first commit.